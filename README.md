Installation instructions:

Requirements:
1. should have git and npm installed and added to the path in your system.
    (You can check it by typing git followed by npm command in command prompt/terminal, if you see the git as well as npm version, it means these are installed in your sysytem)
2. Install 'http-server' globally using npm
    (Simply run the command:  npm install http-server -g )

Steps:
 1. clone the repository
 commands: 
 git clone http://gitlab.scss.tcd.ie/khatriy/Visualization-of-Recorded-Crime-Offences-by-Garda-Divisions.git
 cd Visualization-of-Recorded-Crime-Offences-by-Garda-Divisions
 
 2. Run the http server in the root directory of the project
 commands:
 http-server (open terminal in the current directory and run this command, it should start a local developmemt servr at localhost:8080)
 
 3. Run in internet browser(Chrome recommended)
 just visit url: localhost:8080 (Port might different which you can see in the terminal just after running the http-server)
 
If you face any problems in any of above steps please feel free to contant for issue resolution.
 
